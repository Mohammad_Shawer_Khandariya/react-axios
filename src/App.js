import React from 'react';
import User from './Components/User';
import Posts from './Components/Posts';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';

export default class App extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={User} />
          <Route exact path="/posts/:id" component={Posts} />
        </Switch>
      </Router>
    )
  }
}
