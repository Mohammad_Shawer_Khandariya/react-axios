import axios from "axios";
import React from "react";

export default class Posts extends React.Component {

    constructor(props) {
        super(props);
        console.log(this.props.match.params.id);
        this.state = {
            id: this.props.match.params.id,
            postsData: [],
            commentsData: [],
            postsError: "",
            commentsError: "",
        }
    }

    componentDidMount() {
        axios.get(`https://jsonplaceholder.typicode.com/users/${this.props.match.params.id}/posts`)
            .then((res) => this.setState({
                postsData: res.data,
            }))
            .catch((err) => this.setState({
                postsError: "Data not found, Please try again later!",
            }))
    }

    async getComments(id) {
        console.log(id);
        axios.get(`https://jsonplaceholder.typicode.com/posts/${id}/comments`)
            .then((res) => {
                this.setState({
                    commentsData: res.data,
                })
            })
            .catch((err) => {
                this.setState({
                    commentsError: "Data not found, Please try again later!"
                })
            })
    }

    render() {
        console.log(this.props.location.id);
        return (
            <ul>
                {this.state.postsData.map((post) => {
                    return (
                        <ul key={post.id}>
                            <li>{JSON.stringify(post)}</li>
                            <li>{post.title}</li>
                            <button onClick={() => this.getComments(post.id)}>Comments</button>
                            <ul>
                                {
                                    (this.state.commentsData.length) ?
                                        this.state.commentsData.map((comment) => {
                                            return (
                                                <li key={comment.id}>{JSON.stringify(comment)}</li>
                                            )
                                        }) : ""
                                }
                            </ul>
                        </ul>
                    )
                })}
            </ul>
        )
    }
}