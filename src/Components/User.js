import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import './user.css';

export default class User extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userData: [],
            userError: "",
            postsData: "",
            postsError: "",
        }
    }
    componentDidMount() {
        axios.get("https://jsonplaceholder.typicode.com/users")
            .then((res) => {
                this.setState({
                    userData: res.data,
                })
            })
            .catch((err) => {
                this.setState({
                    userError: "Data not found!"
                })
            })
    }

    getPosts(id) {
        axios.get(`https://jsonplaceholder.typicode.com/users/${id}/posts`)
            .then((res) => this.setState({
                postsData: res.data
            }))
            .catch((err) => this.setState({
                postsError: "Posts not found"
            }))
    }

    render() {
        return (
            <ul className="user-container">
                {this.state.userData.map((user) => {
                    return (
                        <ul key={user.id}>
                            <li id={user.id}
                                onClick={(event) => this.getPosts(event.target.id)} >
                                {user.name}
                            </li>
                            <Link to={{
                                pathname: `/posts/${user.id}`,
                            }} >See Profile</Link>
                        </ul>
                    )
                })}
            </ul>
        )
    }
}